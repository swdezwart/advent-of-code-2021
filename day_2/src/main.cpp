#include "read_input.h"
#include <iostream>

int main() {

    std::vector<std::string> input = read_input();
    int space_pos;
    std::string direction;
    int steps;
    int horizontal_position = 0;
    int vertical_position = 0;
    int aim = 0;

    for (auto line: input) {
        space_pos = line.find_first_of(' ');
        steps = std::stoi(line.substr(space_pos + 1));
        direction = line.substr(0, space_pos);

        if (direction == "forward") {
            horizontal_position += steps;
            vertical_position += steps*aim;
        } else if (direction == "up") {
            aim -= steps;
        } else if (direction == "down") {
            aim += steps;
        }
    }

    std::cout << "position: " << horizontal_position * vertical_position << std::endl;}



