#include "read_input.h"
#include <iostream>
#include <map>
#include <cmath>
#include <bits/stdc++.h>

std::map<char, std::vector<bool>> hex_to_binary = {
        {'0', std::vector<bool>{0, 0, 0, 0}},
        {'1', std::vector<bool>{0, 0, 0, 1}},
        {'2', std::vector<bool>{0, 0, 1, 0}},
        {'3', std::vector<bool>{0, 0, 1, 1}},
        {'4', std::vector<bool>{0, 1, 0, 0}},
        {'5', std::vector<bool>{0, 1, 0, 1}},
        {'6', std::vector<bool>{0, 1, 1, 0}},
        {'7', std::vector<bool>{0, 1, 1, 1}},
        {'8', std::vector<bool>{1, 0, 0, 0}},
        {'9', std::vector<bool>{1, 0, 0, 1}},
        {'A', std::vector<bool>{1, 0, 1, 0}},
        {'B', std::vector<bool>{1, 0, 1, 1}},
        {'C', std::vector<bool>{1, 1, 0, 0}},
        {'D', std::vector<bool>{1, 1, 0, 1}},
        {'E', std::vector<bool>{1, 1, 1, 0}},
        {'F', std::vector<bool>{1, 1, 1, 1}},
};

ulong vect_bool_to_int(std::vector<bool> input) {
    ulong output_int = 0;
    for (int i = 0; i < input.size(); i++) {
        ulong power = pow(2, i);
        output_int += input[input.size() - 1 - i] * power;
    }
    return output_int;
}

class Decoder {
public:
    int total_versions = 0;
    ulong value;

    Decoder(std::string &input) {
        std::vector<bool> input_bits;
        for (auto &c: input) {
            std::vector<bool> subbits = hex_to_binary[c];
            input_bits.insert(input_bits.end(), subbits.begin(), subbits.end());
        }
        std::pair<std::vector<bool>, ulong> decode_output = decode(input_bits);
        value = decode_output.second;
    }

private:
    std::pair<std::vector<bool>, ulong> decode(const std::vector<bool>& bits) {
        int version = vect_bool_to_int(std::vector<bool>(bits.begin(), bits.begin() + 3));
        total_versions += version;
        int type = vect_bool_to_int(std::vector<bool>(bits.begin() + 3, bits.begin() + 6));
        std::vector<bool> remaining_bits;
        ulong value;
        if (type == 4) {
            std::vector<bool> number_bits;
            int subbit_index = 6;
            std::vector<bool> sub_number_bits = std::vector<bool>(bits.begin() + subbit_index,
                                                                  bits.begin() + subbit_index + 5);
            while (sub_number_bits[0]) {
                number_bits.insert(number_bits.end(), sub_number_bits.begin() + 1, sub_number_bits.end());
                subbit_index += 5;
                sub_number_bits = std::vector<bool>(bits.begin() + subbit_index, bits.begin() + subbit_index + 5);
            }
            number_bits.insert(number_bits.end(), sub_number_bits.begin() + 1, sub_number_bits.end());
            value = vect_bool_to_int(number_bits);
            remaining_bits = std::vector<bool>(bits.begin() + subbit_index + 5, bits.end());
        } else {
            bool length_type = bits[6];
            std::pair<std::vector<bool>, ulong> decode_output;
            std::vector<ulong> values;
            if (length_type) {
                int num_subpackets = vect_bool_to_int(std::vector<bool>(bits.begin() + 7, bits.begin() + 18));
                remaining_bits = std::vector<bool> (bits.begin() + 18, bits.end());
                for (int i = 0; i < num_subpackets; i++) {
                    decode_output = decode(remaining_bits);
                    remaining_bits = decode_output.first;
                    values.push_back(decode_output.second);
                }
            } else {
                int num_bits_subpackets = vect_bool_to_int(std::vector<bool>(bits.begin() + 7, bits.begin() + 22));
                remaining_bits = std::vector<bool>(bits.begin() + 22, bits.begin() + 22 + num_bits_subpackets);
                while (!remaining_bits.empty()) {
                    decode_output = decode(remaining_bits);
                    remaining_bits = decode_output.first;
                    values.push_back(decode_output.second);
                }
                remaining_bits = std::vector<bool>(bits.begin() + 22 + num_bits_subpackets, bits.end());
            }
            value = get_value_from_operator(values, type);
        }
        return std::make_pair(remaining_bits, value);
    }

    ulong get_value_from_operator(std::vector<ulong> values, int type){
        ulong value;
        switch (type){
            case 0:
                value = 0;
                for (auto v : values){
                    value+=v;
                }
                break;
            case 1:
                value = 1;
                for (auto v : values){
                    value*=v;
                }
                break;
            case 2:
                value = *std::min_element(values.begin(), values.end());
                break;
            case 3:
                value = *std::max_element(values.begin(), values.end());
                break;
            case 5:
                if (values[0] > values[1]){
                    value = 1;
                }
                else{
                    value = 0;
                }
                break;
            case 6:
                if (values[0] < values[1]){
                    value = 1;
                }
                else{
                    value = 0;
                }
                break;
            case 7:
                if (values[0] == values[1]){
                    value = 1;
                }
                else{
                    value = 0;
                }
                break;
        }
        return value;
    }
};

int main() {
    auto input = read_input();
    Decoder decoder(input[0]);
    std::cout << "Answer: " << decoder.value << std::endl;
}






