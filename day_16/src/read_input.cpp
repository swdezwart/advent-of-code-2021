#include <fstream>
#include "read_input.h"

using namespace std;

vector<string> read_input(){
    ifstream inFile;
    inFile.open("../input", ios::in);
    
    vector<string> input_vec;

    if (inFile.is_open()){
        string line;
        while(getline(inFile, line)){
                input_vec.push_back(line);
        }
        inFile.close();
    }
    return input_vec;
}
	