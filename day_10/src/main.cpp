#include "read_input.h"
#include <boost/algorithm/string.hpp>
#include <map>
#include <iostream>


std::map<char, char> close_map = {
        {'(', ')'},
        {'[', ']'},
        {'{', '}'},
        {'<', '>'}
};

std::map<char, int> score_map1 = {
        {')', 3},
        {']', 57},
        {'}', 1197},
        {'>', 25137}
};

std::map<char, int> score_map2 = {
        {')', 1},
        {']', 2},
        {'}', 3},
        {'>', 4}
};

std::pair<int, long> get_scores(std::vector<std::string> &input){

    int illegal_score = 0;
    std::vector<long> missing_scores;
    for (auto& text : input){
        std::vector<char> unclosed_chars;
        bool illegal = false;
        for(auto& c : text){
            if (close_map.find(c) == close_map.end()){
                if (close_map[unclosed_chars.back()] == c){
                    unclosed_chars.pop_back();
                }
                else{
                    illegal = true;
                    illegal_score += score_map1[c];
                    break;
                }
            }
            else{
                unclosed_chars.push_back(c);
            }
        }
        if (!illegal){
            long mscore = 0;
            for (int i = unclosed_chars.size() - 1; i >= 0 ; i--){
                mscore *= 5;
                mscore += score_map2[close_map[unclosed_chars[i]]];
            }
            missing_scores.push_back(mscore);
        }
    }
    sort(missing_scores.begin(), missing_scores.end());
    return std::make_pair(illegal_score, missing_scores[int(missing_scores.size()/2)]);
}

int main() {

    auto input = read_input();

    std::cout << "Illegal score: " << get_scores(input).first << std::endl;
    std::cout << "Missing score: " << get_scores(input).second << std::endl;

}






