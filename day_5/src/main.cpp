#include "read_input.h"
#include <iostream>
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string/regex.hpp>

std::vector<int> text_to_coordinates(std::string &text) {
    std::vector<std::string> svec_total;
    std::vector<std::string> svec_left;
    std::vector<std::string> svec_right;

    boost::algorithm::split_regex(svec_total, text, boost::regex(" -> "));
    boost::split(svec_left, svec_total[0], boost::is_any_of(","));
    boost::split(svec_right, svec_total[1], boost::is_any_of(","));

    return std::vector<int>{std::stoi(svec_left[0]), std::stoi(svec_left[1]), std::stoi(svec_right[0]),
                            std::stoi(svec_right[1])};
}

void fill_vents(std::vector<std::vector<int>> &vents, std::vector<std::string> &input) {
    for (auto line: input) {
        std::vector<int> coos = text_to_coordinates(line);
        std::pair<int, int> coos_diff;
        coos_diff.first = coos[2] - coos[0];
        coos_diff.second = coos[3] - coos[1];
        int x_sign = (coos_diff.first > 0) - (coos_diff.first < 0);
        int y_sign = (coos_diff.second > 0) - (coos_diff.second < 0);

        if (x_sign == 0) {
            int x = coos[0];
            for (int y = coos[1]; y != coos[3]; y += y_sign) {
                vents[x][y]++;
            }
        } else if (y_sign == 0) {
            int y = coos[1];
            for (int x = coos[0]; x != coos[2]; x += x_sign) {
                vents[x][y]++;
            }
        } else {
            int y = coos[1];
            for (int x = coos[0]; x != coos[2]; x += x_sign, y += y_sign) {
                vents[x][y]++;
            }
        }
        vents[coos[2]][coos[3]]++;
    }
}

int get_num_overlapping_lines(const std::vector<std::vector<int>> &vents) {
    int sum = 0;
    for (auto &row: vents) {
        for (auto &vent: row) {
            if (vent >= 2) {
                sum++;
            }
        }
    }
    return sum;
}

int main() {

    auto input = read_input();

    std::vector<std::vector<int>> vents(1000, std::vector<int>(1000, 0));

    std::vector<int> test = text_to_coordinates(input[0]);

    fill_vents(vents, input);

    std::cout << "Overlapping pipes with diagonal: " << get_num_overlapping_lines(vents) << std::endl;


}






