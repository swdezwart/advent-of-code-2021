#include "read_input.h"
#include <iostream>
#include <boost/algorithm/string.hpp>


std::vector<ulong> get_fish_from_input(std::string &text) {
    std::vector<std::string> svec;

    boost::algorithm::split(svec, text, boost::is_any_of(","));

    std::vector<ulong> fish(9,0);
    for (auto& sint : svec){
        fish[std::stoi(sint)]++;
    }

    return fish;
}

void cycle_day(std::vector<ulong> &fish){
    ulong newborn = fish[0];
    for (int i = 0; i < fish.size() - 1; i++){
        fish[i] = fish[i+1];
    }
    fish[8] = newborn;
    fish[6] += newborn;
}


int main() {

    auto input = read_input();

    std::vector<ulong> fish = get_fish_from_input(input[0]);

    int days = 256;

    for (int i = 0; i < days; i++){
        cycle_day(fish);
    }

    ulong fish_pop = 0;
    for (auto fp : fish){
        fish_pop += fp;
    }

    std::cout << "Total fish: " << fish_pop << std::endl;
}






