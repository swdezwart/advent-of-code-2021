#include "read_input.h"
#include <iostream>
#include <boost/algorithm/string.hpp>

struct Bingo {
    std::vector<int> numbers;
    std::vector<std::vector<std::vector<std::pair<int, bool>>>> boards;
};

Bingo create_bingo(const std::vector<std::string> &input) {
    Bingo bingo;

    std::vector<std::string> svec;

    boost::split(svec, input[0], boost::is_any_of(","));
    for (auto &sint: svec) {
        bingo.numbers.push_back(std::stoi(sint));
    }

    std::vector<std::string> row_str;
    std::vector<std::pair<int, bool>> row;
    std::vector<std::vector<std::pair<int, bool>>> board;

    for (int i = 2; i < input.size(); i++) {
        if (input[i].empty()) {
            bingo.boards.push_back(board);
            board.clear();
        } else {
            boost::split(row_str, input[i], boost::is_any_of(" "));
            for (auto &sint: row_str) {
                if (!sint.empty()) {
                    row.push_back(std::make_pair(std::stoi(sint), 0));
                }
            }
            row_str.empty();
            board.push_back(row);
            row.clear();
        }
    }
    bingo.boards.push_back(board);
    return bingo;
}

bool is_winner(std::vector<std::vector<std::pair<int, bool>>> &board, int row, int column) {
    bool winner_row = true;
    for (auto &field: board[row]) {
        if (!field.second) {
            winner_row = false;
        }
    }
    bool winner_column = true;
    for (auto &r: board) {
        if (!r[column].second) {
            winner_column = false;
        }
    }
    return (winner_row | winner_column);
}

int calculate_score(std::vector<std::vector<std::pair<int, bool>>> &board, int number) {
    int sum_unmarked = 0;

    for (auto &row: board) {
        for (auto &field: row) {
            if (!field.second) {
                sum_unmarked += field.first;
            }
        }
    }
    return sum_unmarked * number;
}

int play_bingo(Bingo &bingo, bool play_for_last) {
    std::vector<bool> winning_boards(bingo.boards.size(), true);
    for (auto number: bingo.numbers) {
        for (auto boardp = bingo.boards.begin(); boardp != bingo.boards.end();) {
            bool erased = false;
            std::vector<std::vector<std::pair<int, bool>>> &board = *boardp;
            for (int i = 0; i < board.size(); i++) {
                for (int j = 0; j < board[i].size(); j++) {
                    if (board[i][j].first == number) {
                        board[i][j].second = true;
                        if (is_winner(board, i, j)) {
                            if (!play_for_last) {
                                return calculate_score(board, number);
                            } else {
                                if (bingo.boards.size() == 1) {
                                    return calculate_score(board, number);
                                }
                                erased = true;
                                boardp = bingo.boards.erase(boardp);
                            }
                        }
                    }
                }
            }
            if (!erased) {
                ++boardp;
            }
        }
    }
}

int main() {

    auto input = read_input();
    Bingo bingo = create_bingo(input);

    int score_first = play_bingo(bingo, false);
    std::cout << "Final score: " << score_first << std::endl;

    int score_last = play_bingo(bingo, true);
    std::cout << "Final score last board: " << score_last << std::endl;

}






