#include "read_input.h"
#include <iostream>
#include <boost/algorithm/string.hpp>
#include <set>

struct Paper{
    std::set<std::pair<int, int>> points;
    std::vector<std::pair<char, int>> instructions;
};

Paper get_paper_from_input(std::vector<std::string> &input) {
    Paper paper;

    bool instr = false;
    for (auto &line: input){
        std::vector<std::string> svec;
        if (!instr){
            if (line.empty()){
                instr = true;
                continue;
            }
            boost::algorithm::split(svec, line, boost::is_any_of(","));
            paper.points.insert(std::make_pair(std::stoi(svec[0]), std::stoi(svec[1])));
        }
        else{
            boost::algorithm::split(svec, line, boost::is_any_of("="));
            paper.instructions.push_back(std::make_pair(svec[0].back(), std::stoi(svec[1])));
        }
    }

    return paper;
}

std::set<std::pair<int, int>> fold_paper(Paper &paper, int n){
    std::set<std::pair<int, int>> prev_points = paper.points;
    std::set<std::pair<int, int>> new_points;
    for (int i = 0; i < std::min(n, int(paper.instructions.size())); i++){
        new_points.clear();
        for (auto &p : prev_points){
            std::pair<int, int> new_p;
            if (paper.instructions[i].first == 'x'){
                if (p.first < paper.instructions[i].second){
                    new_p = p;
                }
                else{
                    new_p.second = p.second;
                    new_p.first = 2*paper.instructions[i].second - p.first;

                }
            }
            else{
                if (p.second < paper.instructions[i].second){
                    new_p = p;
                }
                else{
                    new_p.first = p.first;
                    new_p.second = 2*paper.instructions[i].second - p.second;


                }
            }
            new_points.insert(new_p);
        }
        prev_points = new_points;
    }
    return new_points;
}

void print_code(const std::set<std::pair<int, int>>& code){
    std::cout << "Code: " << std::endl;
    for (int i =0; i < 40; i++){
        for (int j =0; j < 10; j++){

            if (code.find(std::make_pair(i,j)) != code.end()){
                std::cout << "#";
            }
            else{
                std::cout << " ";
            }
        }
        std::cout << "\n";
    }
}


int main() {

    auto input = read_input();

    Paper paper = get_paper_from_input(input);

    std::set<std::pair<int, int>> code1 = fold_paper(paper, 1);

    std::cout << "Points: " << code1.size() << std::endl;

    std::set<std::pair<int, int>> code8 = fold_paper(paper, 100);

    print_code(code8);


}






