#include "read_input.h"
#include <boost/algorithm/string.hpp>
#include <map>
#include <iostream>

bool is_lower(std::string str){
    for (auto c : str){
        if (std::isupper(c)){
            return false;
        }
    }
    return true;
}

int count_str_in_vec(std::string &str_a, std::vector<std::string> &vec){
    int sum = 0;
    for (auto &str_b : vec){
        if (str_a == str_b){
            sum++;
        }
    }
    return sum;
}

class Cave{
public:
    std::map<std::string, std::vector<std::string>> cmap;
    int num_paths = 0;
    Cave(std::vector<std::string> &input){
        for (auto &line: input){
            std::vector<std::string> svec;
            boost::algorithm::split(svec, line, boost::is_any_of("-"));

            for (int i : {0 , 1}){
                int ia = 0+i;
                int ib = abs(-1+i);

                if (svec[ia] != "end" && svec[ib] != "start"){
                    if (cmap.find(svec[ia]) == cmap.end()) {
                        std::vector<std::string> room;
                        room.push_back(svec[ib]);
                        cmap.insert({svec[ia], room});
                    } else {
                        cmap[svec[ia]].push_back(svec[ib]);
                    }
                }
            }
        }
    };

    void find_path(std::vector<std::string> &prev_rooms, int n_small_rooms_allowed){
        if (prev_rooms.back() == "end"){
            num_paths++;
        }
        std::vector<std::string>& next_rooms = cmap[prev_rooms.back()];
        for (auto &next_room : next_rooms){
            if (is_lower(next_room)) {
                if (count_str_in_vec(next_room, prev_rooms) >= n_small_rooms_allowed) {
                    continue;
                }
            }

            std::vector<std::string> rooms = prev_rooms;
            rooms.push_back(next_room);
            if (is_lower(next_room) && count_str_in_vec(next_room, prev_rooms) == 1){
                find_path(rooms, 1);
            }
            else{
                find_path(rooms, n_small_rooms_allowed);
            }

        }
    }
};

int main() {

    auto input = read_input();

    Cave cave(input);

    std::vector<std::string> rooms;
    rooms.push_back("start");
    cave.find_path(rooms, 1);
    std::cout << "Paths for small caves once: " << cave.num_paths << std::endl;
    cave.num_paths = 0;
    cave.find_path(rooms, 2);
    std::cout << "Paths for small caves twice: " << cave.num_paths << std::endl;
}






