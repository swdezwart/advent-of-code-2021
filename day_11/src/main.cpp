#include "read_input.h"
#include <boost/algorithm/string.hpp>
#include <map>
#include <iostream>
#include <algorithm>

class Dumbos {
public:
    std::vector<std::vector<int>> energies;
    std::vector<std::vector<bool>> flashed;
    int flash_n = 0;
    int all_flashed_n = 0;

    Dumbos(std::vector<std::string> &input){
        for (auto &line: input) {
            std::vector<int> row;
            for (auto &c: line) {
                row.push_back(int(c - '0'));
            }
          energies.push_back(row);
        }
    }

    void flash_dumbo(int x, int y) {
        if (!flashed[x][y]) {
            energies[x][y]++;
            if (energies[x][y] > 9) {
                flashed[x][y] = true;
                flash_n++;
                energies[x][y] = 0;
                for (int i: {-1, 0, 1}) {
                    for (int j: {-1, 0, 1}) {
                        if (x + i >= 0 && x + i < energies.size() && y + j >= 0 && y + j < energies[0].size() &&
                            !(i == 0 && j == 0)) {
                            flash_dumbo(x + i, y + j);
                        }
                    }
                }
            }
        }
    }

    int flash_dumbos(int steps){
        for (int i = 0; i < steps; i++){
            flashed = std::vector<std::vector<bool>>(energies.size(), std::vector<bool>(energies[0].size(), false));
            for (int j = 0; j < energies.size(); j++){
                for (int k = 0; k < energies[0].size(); k++){
                    flash_dumbo(j,k);
                }
            }
            if (all_flashed_n == 0 && all_flashed()){
                all_flashed_n = i + 1;
            }
        }
        return flash_n;
    }

    bool all_flashed(){
        for (auto & row: flashed){
            for (bool flash : row){
                if (!flash){
                    return false;
                }
            }
        }
        return true;
    }
};

int main() {

    auto input = read_input();

    Dumbos dumbos(input);
    
    int steps = 500;

    int flash_n = dumbos.flash_dumbos(steps);

    std::cout << "Flashed: " << flash_n << std::endl;
    std::cout << "All flashed: " << dumbos.all_flashed_n << std::endl;

}






