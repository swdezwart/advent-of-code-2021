#include "read_input.h"
#include <iostream>
#include <bits/stdc++.h>
#include <regex>

class ProbeLauncher {
public:
    std::vector<int> target_area;
    int max_upward_vel;

    ProbeLauncher(std::string &input_str) {
        std::regex e("([-\\d]+)");

        std::smatch regex_result;
        std::string buffer = input_str;
        while (std::regex_search(buffer, regex_result, e)) {
            target_area.push_back(std::stoi(regex_result[1]));
            buffer = regex_result.suffix().str(); // Proceed to the next match
        }
        max_upward_vel = -target_area[2] - 1;

    }

    int get_max_height() {
        return max_upward_vel * (max_upward_vel + 1) / 2;
    }

    int brute_force_combinations() {
        int n;
        for (int i = 1; i <= target_area[1]; i++) {
            for (int j = target_area[2]; j <= max_upward_vel; j++) {
                int dx = i;
                int dy = j;
                int x = 0;
                int y = 0;
                while (true) {
                    x += dx;
                    y += dy;
                    if (x > target_area[1]) {
                        break;
                    }
                    if (dx == 0 && x < target_area[0]) {
                        break;
                    }
                    if (y < target_area[2]) {
                        break;
                    }
                    if (x >= target_area[0] && y <= target_area[3]) {
                        n++;
                        break;
                    }
                    if (dx != 0) {
                        dx--;
                    }
                    dy--;
                }
            }
        }
        return n;
    }
};

int main() {
    auto input = read_input();
    ProbeLauncher launcher(input[0]);

    std::cout << "Answer 1: " << launcher.get_max_height() << std::endl;
    std::cout << "Answer 2: " << launcher.brute_force_combinations() << std::endl;

}






