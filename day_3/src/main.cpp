#include "read_input.h"
#include <iostream>
#include <bitset>
#include <string>

std::vector<int> sum_bits_on_position(std::vector<std::string> &input) {
    std::vector<int> bit_sum(12, 0);
    for (auto &line: input) {
        for (int i = 0; i < line.size(); i++) {
            bit_sum[i] += line[i] - '0';
        }
    }
    return bit_sum;
}

std::bitset<12> get_common_bits(std::vector<std::string> &input, bool most_common) {
    std::vector<int> bit_sum = sum_bits_on_position(input);
    std::bitset<12> common;

    for (int i = 0; i < bit_sum.size(); i++) {
        if (most_common){
            if (bit_sum[i] >= float(input.size()) / 2) {
                common.set(bit_sum.size() - 1 - i);
            }
        }
        else{
            if (bit_sum[i] < float(input.size()) / 2) {
                common.set(bit_sum.size() - 1 - i);
            }
        }


    }
    return common;
}

std::bitset<12> get_similar_bits(std::vector<std::string> input, bool most_common) {

    std::string reference_str = get_common_bits(input, most_common).to_string();

    for (int i = 0; i < reference_str.size(); i++){

        for(auto j = input.begin(); j != input.end();)
        {
            if (input.size() == 1)
            {
                return std::bitset<12>(input[0]);
            }
            std::string current_bits = *j;
            if(current_bits[i] != reference_str[i])
            {
                j = input.erase(j);
            }
            else
            {
                ++j;
            }

        }
        reference_str = get_common_bits(input,most_common).to_string();
    }
}

int main() {

    auto input = read_input();

    std::bitset<12> most_common = get_common_bits(input,1);

    std::bitset<12> least_common = get_common_bits(input,0);

    std::cout << "Power consumption: " << most_common.to_ulong() * least_common.to_ulong() << std::endl;

    ulong oxigen = get_similar_bits(input, 1).to_ulong();
    ulong co2 = get_similar_bits(input, 0).to_ulong();

    std::cout << "Life support rating: " << co2 * oxigen << std::endl;
}






