#include "read_input.h"
#include <iostream>

int main() {

    std::vector<std::string> input = read_input();

    int total_increased = 0;
    int window_size = 3;
    int previous_window_sum = 0;
    int window_sum;

    for (int i = 0; i <= input.size() - window_size; i++) {

        window_sum = 0;
        for (int j = 0; j < window_size; j++) {
            window_sum += std::stoi(input[i + j]);
        }
        if (window_sum > previous_window_sum && i != 0) {
            total_increased++;
        }
        previous_window_sum = window_sum;
    }

    std::cout << total_increased;
}



