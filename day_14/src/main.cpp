#include "read_input.h"
#include <iostream>
#include <map>
#include <boost/algorithm/string/regex.hpp>
#include <boost/regex.hpp>

class Polymer {
public:
    std::map<char, long> letter_count;
    std::map<std::string, char> pair_map;
    std::map<std::string, long> pair_count;

    Polymer(std::vector<std::string> &input) {
        bool instr = false;
        for (auto &line: input) {

            if (!instr) {
                if (line.empty()) {
                    instr = true;
                    continue;
                }
                for (int i = 0; i < line.size(); i++) {
                    char c = line[i];
                    if (letter_count.find(c) != letter_count.end()) {
                        letter_count[c]++;
                    } else {
                        letter_count[c] = 1;
                    }

                    if (i != line.size() - 1) {
                        std::string pair = line.substr(i, 2);
                        if (pair_count.find(pair) != pair_count.end()) {
                            pair_count[pair]++;
                        } else {
                            pair_count[pair] = 1;
                        }
                    }
                }
            } else {
                std::vector<std::string> svec;
                boost::algorithm::split_regex(svec, line, boost::regex(" -> "));
                pair_map[svec[0]] = svec[1].back();
            }
        }
    }

    void pair_insertion(int n) {
        std::map<std::string, long> old_pair_count;
        for (int i = 0; i < n; i++) {
            old_pair_count = pair_count;
            for (auto &[pair, _]: old_pair_count) {
//                add to letter count
                char c = pair_map[pair];
                if (letter_count.find(c) != letter_count.end()) {
                    letter_count[c] += old_pair_count[pair];
                } else {
                    letter_count[c] = old_pair_count[pair];
                }
//                create new pairs
                std::string pair_1 = std::string(1, pair[0]) + pair_map[pair];
                std::string pair_2 = std::string(1, pair_map[pair]) + pair[1];
                pair_count[pair] -= old_pair_count[pair];
                for (auto p: {pair_1, pair_2}) {
                    if (pair_count.find(p) != pair_count.end()) {
                        pair_count[p] += old_pair_count[pair];
                    } else {
                        pair_count[p] = old_pair_count[pair];
                    }
                }
            }
            old_pair_count.clear();
        }
    }


};

long subtr_least_from_most_common(std::map<char, long> letter_count) {
    long min = letter_count.begin()->second;
    long max = 0;
    for (auto &[_, count]: letter_count) {
        if (count > max) {
            max = count;
        }
        if (count < min) {
            min = count;
        }
    }
    return max - min;
}


int main() {
    auto input = read_input();
    Polymer polymer(input);

    polymer.pair_insertion(10);
    std::cout << "Answer 1: " << subtr_least_from_most_common(polymer.letter_count) << std::endl;

    polymer.pair_insertion(30);
    std::cout << "Answer 2: " << subtr_least_from_most_common(polymer.letter_count) << std::endl;
}






