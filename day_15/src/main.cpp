#include "read_input.h"
#include <iostream>
#include <map>
#include <boost/algorithm/string/regex.hpp>
#include <boost/regex.hpp>
#include <queue>

int MAX = 999999999;

struct Node{
    int risk;
    int total_risk;
    bool goal = false;
    std::vector<Node*> neighbours;
};

struct CmpNodePtrs{
    bool operator()(const Node* lhs, const Node* rhs) const{
        return lhs->total_risk > rhs->total_risk;
    }
};

class Chitons {
public:
    std::vector<std::vector<Node>> nodes;

    Chitons(std::vector<std::string> &input, int multiplier) {
        for (int k = 0; k < multiplier; k++) {
            for (int i = 0; i < input.size(); i++) {
                std::vector<Node> node_row;
                for (int l = 0; l < multiplier; l++) {
                    for (int j = 0; j < input[0].size(); j++) {
                        int risk = int(input[i][j] - '0') + k + l;
                        risk = (risk - 1) % 9 + 1;
                        Node node;
                        node.risk = risk;
                        node.total_risk = MAX;
                        node_row.push_back(node);
                    }
                }
                nodes.push_back(node_row);
            }
        }
        set_neighbours();
    }

    void set_neighbours(){
        for (int i = 0; i < nodes.size(); i++){
            for (int j = 0; j < nodes[0].size(); j++){
                if (i != 0) {
                    nodes[i][j].neighbours.push_back(&nodes[i-1][j]);
                }
                if (j != 0) {
                    nodes[i][j].neighbours.push_back(&nodes[i][j-1]);
                }
                if (i != nodes.size()-1) {
                    nodes[i][j].neighbours.push_back(&nodes[i+1][j]);
                }
                if (j != nodes[0].size()) {
                    nodes[i][j].neighbours.push_back(&nodes[i][j+1]);
                }
            }
        }
    }

    int find_path(){
        std::priority_queue<Node*, std::vector<Node*>, CmpNodePtrs> pqueue;
        nodes[0][0].total_risk=0;
        pqueue.push(&nodes[0][0]);
        nodes[nodes.size()-1][nodes[0].size()-1].goal = true;

        while (!pqueue.empty()){
            Node current_node = *pqueue.top();
            pqueue.pop();
            for (auto neighbour : current_node.neighbours){
                int new_risk = current_node.total_risk + neighbour->risk;
                if (neighbour->total_risk > new_risk){
                    neighbour->total_risk = new_risk;
                    if (neighbour->goal){
                        return neighbour->total_risk;
                    }
                    pqueue.push(neighbour);
                }
            }
        }
        return MAX;
    }
};

int main() {
    auto input = read_input();
    Chitons chitons(input, 5);

    int len = chitons.find_path();
    std::cout << "Answer: " << len << std::endl;
}






