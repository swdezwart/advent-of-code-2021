# cmake version to be used
cmake_minimum_required( VERSION 3.0 )

# project name
project( day_7 )


set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)
find_package( Boost REQUIRED COMPONENTS regex )

# include
#include_directories(./src ${Boost_INCLUDE_DIRS})

if(Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIRS})
    add_executable( binary ./src/main.cpp ./src/read_input.h ./src/read_input.cpp)
    target_link_libraries(binary ${Boost_LIBRARIES})
endif()