#include "read_input.h"
#include <iostream>
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string/regex.hpp>

struct Pattern{
    std::vector<std::bitset<7>> signals;
    std::vector<std::bitset<7>> outputs;
};

std::vector<Pattern> get_patterns_from_input(const std::vector<std::string> &input){
    std::vector<Pattern> patterns;
    for (auto& line : input){
        std::vector<std::string> svec_total;
        std::vector<std::string> signals;
        std::vector<std::string> outputs;

        boost::algorithm::split_regex(svec_total, line, boost::regex(" \\| "));
        boost::split(signals, svec_total[0], boost::is_any_of(" "));
        boost::split(outputs, svec_total[1], boost::is_any_of(" "));

        Pattern pattern;
        for (auto& signal : signals){
            std::bitset<7> signal_bits;
            for (auto&c : signal){
                signal_bits.set(c - 'a');
            }
            pattern.signals.push_back(signal_bits);
        }
        for (auto& output : outputs){
            std::bitset<7> output_bits;
            for (auto&c : output){
                output_bits.set(c - 'a');
            }
            pattern.outputs.push_back(output_bits);
        }

        patterns.push_back(pattern);

    }

    return patterns;

}


std::vector<std::bitset<7>> fill_signal_map(std::vector<std::bitset<7>> &signals){
    std::bitset<7> empty_bits;
    std::vector<std::bitset<7>> signal_map(10);

    for (auto &signal : signals){
        if (signal.count() == 2)
        {
            signal_map[1] = signal;
        }
        if (signal.count() == 3)
        {
            signal_map[7] = signal;
        }
        if (signal.count() == 4)
        {
            signal_map[4] = signal;
        }
        if (signal.count() == 7)
        {
            signal_map[8] = signal;
        }

    }
    for (auto &signal : signals){
        if (signal.count() == 5){
            if ((signal | signal_map[4]).count() == 7) {
                signal_map[2] = signal;
            }
            else if ((signal | signal_map[1]).count() == 5){
                signal_map[3] = signal;
            }
            else {
                signal_map[5] = signal;
            }
        }
        if (signal.count() == 6){
            if ((signal | signal_map[4]).count() == 6){
                signal_map[9] = signal;
            }
            else if ((signal | signal_map[1]).count() == 7){
                signal_map[6] = signal;
            }
            else {
                signal_map[0] = signal;
            }

        }
    }
    return signal_map;
}

int main() {

    auto input = read_input();

    std::vector<Pattern> patterns = get_patterns_from_input(input);

    int output_sum = 0;

    for (auto& pattern : patterns){
        std::string output_str;

        std::vector<std::bitset<7>> smap = fill_signal_map(pattern.signals);
        for (auto& output: pattern.outputs){
            for (int i = 0; i < smap.size(); i++){
                if (output == smap[i]){
                    output_str += std::to_string(i);
                }
            }
        }
        output_sum += std::stoi(output_str);

    }
    std::cout << "Sum: " << output_sum << std::endl;
}






