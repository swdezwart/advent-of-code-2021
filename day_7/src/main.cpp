#include "read_input.h"
#include <iostream>
#include <boost/algorithm/string.hpp>


std::vector<int> get_crabs_from_input(std::string &text) {
    std::vector<std::string> svec;

    boost::algorithm::split(svec, text, boost::is_any_of(","));


    std::vector<int> crabs;
    for (auto& sint : svec){
        crabs.push_back(std::stoi(sint));
    }

    int max_crab = *std::max_element(crabs.begin(), crabs.end());

    std::vector<int> summed_crabs (max_crab+1, 0);

    for (int crab : crabs){
        summed_crabs[crab]++;
    }

    return summed_crabs;
}


int main() {

    auto input = read_input();

    std::vector<int> crabs = get_crabs_from_input(input[0]);

    std::vector<int> fuel_costs(crabs.size(), 0);

    for (int i = 0; i < fuel_costs.size(); i++){
        for (int j = 0; j < crabs.size(); j++){
            for (int k = 0; k < abs(i-j); k++){
                fuel_costs[i] += crabs[j]*(k+1);
            }
        }
    }

    int min_fuel = *std::min_element(fuel_costs.begin(), fuel_costs.end());

    std::cout << "Fuel: " << min_fuel << std::endl;
}






